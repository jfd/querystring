export {create};
export {fromIterable};
export {fromObject};
export {fromUrl};
export {fromSearchPart};
export {get};
export {format};
export {str};

const defaultDecodeOptions = {
    seperatorChar: "&",
    equalChar: "=",
    maxKeys: 100000,
};


function create() {
    return new Map;
}


function fromObject(obj) {
    const qs = new Map;
    const keys = Object.keys(obj);
    List.each(keys, (key) => {
        qs.set(key, obj[key]);
    });
    return qs;
}


function fromIterable(iterable) {
    return new Map(iterable);
}


function fromUrl(url, options=defaultDecodeOptions) {
    const queryIdx = url.indexOf("?");

    if (queryIdx === -1) {
        return new Map;
    }

    return decode(url.substr(queryIdx + 1), options.seperatorChar,
                  options.equalChar, options.maxKeys)
}


function fromSearchPart(input, options=defaultDecodeOptions) {
    const stripped = input[0] === "?" ? input.substr(1) : input;
    return decode(input, options.seperatorChar, options.equalChar)
}


function get(qs, key, defaultValue=null) {
    return qs.has(key) ? qs.get(key) : defaultValue;
}


function format(qs, seperatorChar="&", equalChar="=") {
    const result = [];

    for (let [key, value] of qs) {

      console.log(key + " = " + value);
    }


    if (typeof obj === 'object') {
      return Object.keys(obj).map(function(k) {
        var ks = encodeURIComponent(stringifyPrimitive(k)) + eq;
        if (Array.isArray(obj[k])) {
          return obj[k].map(function(v) {
            return ks + encodeURIComponent(stringifyPrimitive(v));
          }).join(sep);
        } else {
          return ks + encodeURIComponent(stringifyPrimitive(obj[k]));
        }
      }).join(sep);
    return result.join(seperatorChar);
}


function str(qs) {
    return format(qs);
}


// Internals


function decode(raw, seperatorChar, equalChar, maxKeys) {
    const qs = new Map;

    if (raw.length === 0) {
        return qs;
    }

    const spaceRegExp = /\+/g;
    const pairs = raw.split(seperatorChar);
    const len = Math.min(pairs.length, maxKeys);

    for (let idx = 0; idx < len; idx++) {
        const pair = pairs[idx].replace(spaceRegExp, "%20");
        const equalIdx = pair.indexOf(equalChar);

        let keyString, valueString;

        if (equalIdx >= 0) {
          keyString = pair.substr(0, equalIdx);
          valueString = pair.substr(equalIdx + 1);
        } else {
          keyString = pair;
          valueString = "";
        }

        const key = decodeURIComponent(keyString);
        const value = decodeURIComponent(valueString);

        if (qs.has(key) === false) {
            qs.set(key, value);
        } else if (Array.isArray(qs.get(key))) {
            qs.get(key).push(value);
        } else {
            qs.set(key, [qs.get(key), value]);
        }
    }

    return qs;
}


function stringifyPrimitive(value) {
    switch (typeof value) {
    case "string":
        return value;

    case "boolean":
        return value ? "true" : "false";

    case "number":
        return isFinite(value) ? value : "";

    default:
        return "";
    }
}
