

function testStr() {
    const qs = QueryString.fromObject({ abc: 123, def: 434});
    Assert.equal(QueryString.str(qs), "abc=123&def=434");
}
